# QAGENCY  

User manual for installing and starting the application

## Installing project

### Prerequisites:

- Installed [GIT CLI](https://git-scm.com/)
- Installed [Docker](https://docs.docker.com/get-docker/)  

---

We are going to clone repo with command

```bash
git clone https://gitlab.com/GaGiiiii8/qagency.git
```

Enter the project directory

```bash
cd qagency
```

Switch to master branch

```bash
git checkout master
```

Finally start the application by running the command

```bash
docker-compose up -d
```

You can run test by typing the following command (please take in mind that mysql container needs some time to start)

```bash
docker-compose exec app npm test
```

Postman collection is available in file postman.json.  
You can use admin user for testing:
- **email**: admin@admin.com
- **password**: admin

---

# API Documentation  

### Auth

--- 

#### Login

```http
POST /v1/login
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `email` | `string` | **Required** \| Must be valid email address. |
| `password` | `string` | **Required** \| Users password (minimum 4 characters, maximum 30 characters). |

#### Register

```http
POST /v1/register
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `username` | `string` | **Required** \| Users username (minimum 2 characters, maximum 30 characters). |
| `email` | `string` | **Required** \| Must be valid email address. |
| `password` | `string` | **Required** \| Users password (minimum 4 characters, maximum 30 characters). |
| `password_confirmation` | `string` | **Required** \| Should be the same as password. |

### Users

---

#### Get all

```http
GET /v1/users
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `page` (query param) | `integer` | **Optional** \| Current page, default is 1. |
| `perPage` (query param) | `integer` | **Optional** \| Rows per page, default is 10. |
| `search` (query param) | `string` | **Optional** \| String to search for at USERNAME and EMAIL fileds. |
| `orderBy` (query param) | `string` | **Optional** \| Field to order by, default is id. |
| `orderType` (query param) | `string` | **Optional** \| Type of order (desc or asc), default is desc. |

#### Get single user

```http
GET /v1/users/:id
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id` (query param) | `integer` | **Required** \| ID of user that is being requested. |

#### Create new user (admin only)

```http
POST /v1/users
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `username` | `string` | **Required** \| Users username (minimum 2 characters, maximum 30 characters). |
| `email` | `string` | **Required** \| Must be valid email address. |
| `password` | `string` | **Required** \| Users password (minimum 4 characters, maximum 30 characters). |

#### Update user (admin only)

```http
PUT /v1/users/:id
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `id` (query param) | `integer` | **Required** \| Id of the user that should be updated. |
| `username` | `string` | **Required** \| Users username (minimum 2 characters, maximum 30 characters). |
| `email` | `string` | **Required** \| Must be valid email address. |

#### Delete user (admin only)

```http
DELETE /v1/users/:id
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `id` (query param) | `integer` | **Required** \| Id of the user that should be deleted. |

#### Get books for user

```http
GET /v1/users/:id/books
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id` (query param) | `integer` | **Required** \| ID of user that is being requested. |

#### Activate user

```http
GET /v1/users/:id/activate
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id` (query param) | `integer` | **Required** \| ID of user that is being requested. |

#### Deactivate

```http
GET /v1/users/:id/deactivate
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id` (query param) | `integer` | **Required** \| ID of user that is being requested. |


### Books

---

#### Get all

```http
GET /v1/books
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `page` (query param) | `integer` | **Optional** \| Current page, default is 1. |
| `perPage` (query param) | `integer` | **Optional** \| Rows per page, default is 10. |
| `search` (query param) | `string` | **Optional** \| String to search for at USERNAME and EMAIL fileds. |
| `orderBy` (query param) | `string` | **Optional** \| Field to order by, default is id. |
| `orderType` (query param) | `string` | **Optional** \| Type of order (desc or asc), default is desc. |

#### Get single book

```http
GET /v1/books/:id
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id` (query param) | `integer` | **Required** \| ID of book that is being requested. |

#### Create a new book

```http
POST /v1/books
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `user_id` | `integer` | **Required** \| Author of the book. |
| `number_of_pages` | `integer` | **Required** \| Number of pages that book has. |
| `title` | `string` | **Required** \| Title of the book (minimum is 2 characters, maximum 100 characters). |
| `description` | `string` | **Optional** \| Description of the book (minimum 2 characters, maximum 1000 characters). |

#### Update a book

```http
PUT /v1/books/:id
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `user_id` | `integer` | **Required** \| Author of the book. |
| `number_of_pages` | `integer` | **Required** \| Number of pages that book has. |
| `title` | `string` | **Required** \| Title of the book (minimum is 2 characters, maximum 100 characters). |
| `description` | `string` | **Optional** \| Description of the book (minimum 2 characters, maximum 1000 characters). |

#### Delete book

```http
DELETE /v1/books/:id
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `id` (query param) | `integer` | **Required** \| Id of the book that should be deleted. |

---

## Responses

API returns a JSON response in the following format:

```javascript
{
  "error": boolean,
  "message": string,
  "data": data,
  "?token": string,
}
```
The `message` - attribute contains a message commonly used to indicate errors or success messages.

The `data` - attribute contains requested resource/s or processed resource.  

The `errors` - indicates if there are erros present.

The `token` - attribute is optional and it will be returned when user logins.

## Status Codes

API returns the following status codes:

| Status Code | Description |
| :--- | :--- |
| 200 | `OK` |
| 201 | `CREATED` |
| 204 | `NO CONTENT` |
| 400 | `BAD REQUEST` |
| 401 | `UNAUTHORIZED` |
| 404 | `NOT FOUND` |
| 422 | `UNPROCESSABLE CONTENT` |
| 429 | `TOO MANY REQUESTS` |
| 500 | `INTERNAL SERVER ERROR` |

### Due to time limitations I wasn't able to implement next features:
- Typescript
- Swagger documentation
- Caching system (Redis)
- Document code with comments (Documentor)
- Add successful logs
- Add more tests, cover more edge cases, write more assertions
- Finish the CICD process
- Add soft deletes

### Below is the example of CICD I wrote for the other project:

```
stages:
    - test
    - build
    - deploy

run_tests:
    stage: test
    image: php:8.1-fpm
    before_script:
        - apt update 
        - curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
        - apt install libzip-dev -y && apt install zip -y
        - docker-php-ext-install zip mysqli pdo pdo_mysql
        - composer i
        - cp .env.example .env
        - php artisan key:generate
    services:
    - name: mysql:8
    variables:
        MYSQL_DATABASE: testgagi_test
        MYSQL_ROOT_PASSWORD: testgagi
    script:
        - php artisan test

build_image:
    stage: build
    image: docker:20.10.16
    services:
        - docker:20.10.16-dind
    variables:
        IMAGE_NAME: gagiiiii/testgagi_app
        IMAGE_TAG: "1.0"
        DOCKER_TLS_CERTDIR: "/certs"
    before_script:
        - docker login -u $DOCKER_UN -p $DOCKER_PW
    script:
        - docker build -t $IMAGE_NAME:$IMAGE_TAG .
        - docker push $IMAGE_NAME:$IMAGE_TAG

deploy:
    stage: deploy
    image: php:8.1-fpm
    before_script:
        - apt update
        - apt install -y openssh-server
        - apt install -y sshpass
    script:
        - sshpass -p $SERVER_PASSWORD ssh -o StrictHostKeyChecking=no root@$SERVER_IP "cd /var/www/html/testgagi && git checkout master && docker pull gagiiiii/testgagi_app:1.0 && git pull origin master && docker-compose down && docker-compose up -d && docker-compose exec -d app php artisan migrate --force"

```
